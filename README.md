#Instalacion

Configure JAVA_HOME environment variable:
Open the /etc/environment file:
$ sudo atom /etc/environment
$ source /etc/environment
Add this line:
JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"
Open ~/.bashrc:
$ sudo nano ~/.bashrc
Add these lines to the end of the file:
export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
export PATH=$JAVA_HOME/bin:$PATH
$ source ~/.bashrc
$ sudo apt-get install maven
Check the installed maven's version:
$ mvn -version
It will show something like:
Apache Maven 3.0.5
Maven home: /usr/share/maven
Java version: 1.7.0_51, vendor: Oracle Corporation
Java home: /usr/lib/jvm/java-7-openjdk-amd64/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "3.13.0-24-generic", arch: "amd64", family: "unix"

#ERRORES
sudo apt-get install openjdk-8-jdk
